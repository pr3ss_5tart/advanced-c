#include <iostream>
#include "Database.h"

using namespace std;
using namespace Records;


void createDB2 () {
  Database myDB; 
  auto& emp1 = myDB.addEmployee2("Greg", "Wallis", true);
  emp1->fire();
    
  // auto& emp1 = myDB.addEmployee2("Greg", "Wallis", true);
  // emp1->fire();
    
  auto& emp2 = myDB.addEmployee2("Marc", "Gregoire", true);
  emp2->setSalary(100000);
    
  auto& emp3 = myDB.addEmployee2("John", "Doe", false);
  emp3->setSalary(10000);
  emp3->promote();
  
  myDB.saveDB ("db1.txt");
  myDB.saveDB ("db2.txt");
  myDB.saveBinaryDB("db1.dat");
  myDB.saveBinaryDB("db2.dat");
}

int main()
{
  // freopen("db.txt","w",stdout);
  createDB2 ();


  Database myDB;
  auto& emp1 = myDB.addEmployee2("Greg", "Wallis", true);
  emp1->fire();

  myDB.readDB2("db1.dat" );
  myDB.readDB2("db2.dat" );
    
	return 0;
}
/*
*/
