#pragma once

#include <iostream>
#include <vector>
#include "Employee.h"

namespace Records {
  using namespace std;
	const int kFirstEmployeeNumber = 1000;

	class Database
	{

	private:
    std::vector<unique_ptr<Employee>> mEmployees2;
		std::string  mDatabaseFileName = "db.dat";
		int mNextEmployeeNumber;

	public:
		Database();
		// ~Database();

    // copy cstr
    bool addEmployee2( Employee& e) ; 
    unique_ptr<Employee>& addEmployee2(const std::string& firstName,
							  const std::string& lastName, bool hourly = false );

		const Employee* getEmployee(int employeeNumber);
		const auto&     getEmployee(const std::string& firstName, const std::string& lastName);

		void displayAll() const;
    void saveDB( const std::string& fn ) const;
		void saveBinaryDB(const std::string& fn="db.dat") const;
		void readDB2(const std::string& fn="db.dat") ;
		void displayCurrent() const;
		void displayFormer() const;
	  bool isDuplicate(int id) const ;
	};
}
